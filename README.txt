
Mysite is a simple Django project containing an app to list articles and view their details.


Quick start
-----------

1. Add "galepr" to the settings.py file inside "mysite/" folder, INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'galepr',
    ]

2. Include the galepr URLconf in the urls.py inside "mysite/" folder, like this::

    url(r'^article/', include('galepr.urls')),

3. Run `python manage.py migrate` to create the polls models.

4. Visit http://127.0.0.1:8000/article/ to view the galepr app.