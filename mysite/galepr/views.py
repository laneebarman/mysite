from django.conf.urls import include
from django.shortcuts import get_object_or_404, render
from galepr.models import Article

def index(request):
	latest_article_list = Article.objects.order_by('-publication')
	all_article_list = Article.objects.all()
	context = {
		'latest_article_list': latest_article_list,
		'all_article_list': all_article_list
	}
	return render(request, 'galepr/articleList.html', context)
	
	
def detail(request, article_id):
	article_detail = get_object_or_404(Article, pk=article_id)
	all_article_list = Article.objects.all()
	return render(request, 'galepr/articleDetails.html', {'article_detail': article_detail, 'all_article_list': all_article_list })