from django.db import models

class Article(models.Model):
	title = models.CharField(max_length=200)
	author = models.CharField(max_length=200)
	publication = models.DateTimeField()
	category = models.CharField(max_length=200)
	heroImg = models.ImageField()
	optImg = models.ImageField()
	body = models.TextField()
	
	def __str__(self):
		return self.title
