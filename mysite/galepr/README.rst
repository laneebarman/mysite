========
galepr
========

galepr is a simple Django app containing articles of various categories.



Quick start
-----------

1. Add "galepr" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'galepr',
    ]

2. Include the polls URLconf in your project urls.py like this::

    url(r'^article/', include('galepr.urls')),

3. Run `python manage.py migrate` to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create an article (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/article/ to visit the app.